# Sandbox

Sandbox - WordPress Sandbox for Know the Code labs

This WordPress sandbox gives you a local website to play and test as you work through the [Know the Code](https://KnowTheCode.io) labs and content.  It's a sandbox because as kids that's where we played.  

## Features

This site includes:

1. [UpDevTools](https://gitlab.com/uptechlabs/updevtools) - A suite of developer tools for your WordPress development environment.  Yup, you get Kint, Whoops, and much more.
2. [Query Monitor](https://wordpress.org/plugins/query-monitor/) - Reporting tool for queries.  Come on, how can you development and test without this one.

## Installation

1. In your favorite web appliance, create a new website.  You can call it `sandbox.dev` or `ktc-sandbox.dev`.
2. Replace out the `wp-content` folder with this repository by following these steps:
    * In terminal, navigate to `{path to your sandbox project}/`.
    * Then type in terminal: `git clone git@gitlab.com:knowthecode/ktc-sandbox.git`.  The repository is copied into your folder from GitLab.
    * Next move this repositories `wp-content` folder to the root of your project.  Yes, you want to overwrite the current `wp-content` folder.
3. Then type in terminal: `cd plugins/updevtools`
4. Then type in terimal: `composer install`.  Composer will then install all of the updated dependencies in the `assets/vendor` folder.
5. Log into your WordPress website.
6. Go to Plugins and activate the UpDevTools and Query Monitor plugins.

### Dummy Content

Want the same dummy content that Tonya's using?  No problem.  Import the `dummy-content/ktc-sandbox.sql` file into your Sandbox's MySQL database.

### Theme

This sandbox includes the default WordPress theme.  If you want a different one, then load that puppy up yourself.